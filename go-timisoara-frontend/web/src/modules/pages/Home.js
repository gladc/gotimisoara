// Imports
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Helmet } from 'react-helmet'

// UI Imports
import { Grid, GridCell } from '../../ui/grid'
import { H3, H4 } from '../../ui/typography'
import Icon from '../../ui/icon'
import { textLevel1 } from '../../ui/common/shadows'
import { white, grey, grey3 } from '../../ui/common/colors'

// App Imports
import { APP_URL } from '../../setup/config/env'

// Component
const HowItWorks = (props) => (
    <div>
      {/* SEO */}
      <Grid alignCenter={true} style={{
        backgroundImage: `url('${ APP_URL }/images/cover.jpg')`,
        backgroundAttachment: 'fixed',
        backgroundSize: 'cover',
        backgroundPosition: 'center top',
        height: 'calc(100vh - 5em)',
        textAlign: 'center',
        color: white
      }}/>
      <Helmet>
        <title>Go Timisoara</title>
      </Helmet>

      {/* Top title bar */}
      <Grid style={{ backgroundColor: grey }}>
        <GridCell style={{ padding: '2em', textAlign: 'center' }}>
          <H3 font="secondary">Articole adaugate</H3>
        </GridCell>
      </Grid>

        <Grid>
            <GridCell justifyCenter={true} style={{ textAlign: 'center', padding: '8em 0em' }}>

                <H4 style={{ marginTop: '0.5em', textTransform: 'uppercase' }}>Spotlight Heritage 2019</H4>

                <p style={{ marginTop: '0.5em', color: grey3 }}>Cartierul Iosefin și Valeria dr. Pintea</p>
            </GridCell>

            <GridCell style={{ background: `url('${ APP_URL }/images/stock/articles/1.jpg') no-repeat` }}/>
        </Grid>

        <Grid>
            <GridCell style={{ background: `url('${ APP_URL }/images/stock/articles/2.jpg') no-repeat` }}/>

            <GridCell justifyCenter={true} style={{ textAlign: 'center', padding: '8em 0em' }}>

                <H4 style={{ marginTop: '0.5em', textTransform: 'uppercase' }}>PUNCT TERMIC II</H4>

                <p style={{ marginTop: '0.5em', color: grey3 }}>Ateliere transdisciplinare</p>
            </GridCell>
        </Grid>

        <Grid>
            <GridCell justifyCenter={true} style={{ textAlign: 'center', padding: '8em 0em' }}>

                <H4 style={{ marginTop: '0.5em', textTransform: 'uppercase' }}>PUNCT TERMIC I</H4>

                <p style={{ marginTop: '0.5em', color: grey3 }}>Ateliere transdisciplinare</p>
            </GridCell>

            <GridCell style={{ background: `url('${ APP_URL }/images/stock/articles/2.jpg') no-repeat` }}/>
        </Grid>

        <Grid>
            <GridCell justifyCenter={true} style={{ textAlign: 'center', padding: '8em 0em' }}>

                <H4 style={{ marginTop: '0.5em', textTransform: 'uppercase' }}>Ateliere de dans pentru persoane cu deficiențe de auz</H4>
            </GridCell>

            <GridCell style={{ background: `url('${ APP_URL }/images/stock/articles/3.jpg') no-repeat` }}/>
        </Grid>

        <Grid>
            <GridCell style={{ background: `url('${ APP_URL }/images/stock/articles/4.jpg') no-repeat` }}/>

            <GridCell justifyCenter={true} style={{ textAlign: 'center', padding: '8em 0em' }}>

                <H4 style={{ marginTop: '0.5em', textTransform: 'uppercase' }}>LA PAS</H4>

                <p style={{ marginTop: '0.5em', color: grey3 }}>Festival de gastronomie artizanală.</p>
            </GridCell>
        </Grid>

        <Grid>
            <GridCell justifyCenter={true} style={{ textAlign: 'center', padding: '8em 0em' }}>

                <H4 style={{ marginTop: '0.5em', textTransform: 'uppercase' }}>Bega! prezintă The Lightning Project</H4>

            </GridCell>

            <GridCell style={{ background: `url('${ APP_URL }/images/stock/articles/5.jpg') center top no-repeat` }}/>
        </Grid>

    </div>
)

// Component Properties
HowItWorks.propTypes = {
  user: PropTypes.object.isRequired
}

// Component State
function howItWorksState(state) {
  return {
    user: state.user
  }
}

export default connect(howItWorksState, {})(HowItWorks)
