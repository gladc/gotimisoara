// Imports
import React from 'react'
import { Helmet } from 'react-helmet'
import { Link } from 'react-router-dom'

// UI Imports
import { Grid, GridCell } from '../../ui/grid'
import { H3 } from '../../ui/typography'
import { grey } from '../../ui/common/colors'

// App Imports
import { APP_URL } from '../../setup/config/env'
import home from '../../setup/routes/home'

// Component
const NotFound = () => (
  <div>
    {/* SEO */}
    <Helmet>
      <title>Lost? - Crate</title>
    </Helmet>


    <Grid>
      <GridCell style={{ textAlign: 'center' }}>
        <p style={{ textAlign: 'center', marginTop: '2em', marginBottom: '2em' }}>
          <img src={`${ APP_URL }/images/stock/logo/logo.png`} alt="404" style={{ width: '20em' }}/>
        </p>

        <H3 font="secondary">Pagina accesata nu exista....</H3>

        <p style={{ marginTop: '2em' }}>Solutii?</p>

        <p style={{ marginTop: '0.5em' }}>Intoarece-te pe  <Link to={home.home.path}>home page.</Link> </p>
      </GridCell>
    </Grid>
  </div>
)

export default NotFound
