// App Imports
import Home from '../../modules/pages/Home'

// Home routes
export default {
  home: {
    path: '/',
    component: Home,
    exact: true
  }
}
