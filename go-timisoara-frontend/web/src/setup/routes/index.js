// App Imports
import { APP_URL_API } from '../config/env'
import home from './home'

// Combined routes
export const routes = Object.assign( home)

// API Routes
export const routeApi = APP_URL_API

// Image
export const routeImage = APP_URL_API
